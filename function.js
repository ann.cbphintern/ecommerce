//BUTTON +/-
function up1(max) {
    document.getElementById("item1").value = parseInt(document.getElementById("item1").value) + 1;
    if (document.getElementById("item1").value >= parseInt(max)) {
        document.getElementById("item1").value = max;
    }
}
function down1(min) {
    document.getElementById("item1").value = parseInt(document.getElementById("item1").value) - 1;
    if (document.getElementById("item1").value <= parseInt(min)) {
        document.getElementById("item1").value = min;
    }
}

function up2(max) {
    document.getElementById("item2").value = parseInt(document.getElementById("item2").value) + 1;
    if (document.getElementById("item2").value >= parseInt(max)) {
        document.getElementById("item2").value = max;
    }
}
function down2(min) {
    document.getElementById("item2").value = parseInt(document.getElementById("item2").value) - 1;
    if (document.getElementById("item2").value <= parseInt(min)) {
        document.getElementById("item2").value = min;
    }
}

function up3(max) {
    document.getElementById("item3").value = parseInt(document.getElementById("item3").value) + 1;
    if (document.getElementById("item3").value >= parseInt(max)) {
        document.getElementById("item3").value = max;
    }
}
function down3(min) {
    document.getElementById("item3").value = parseInt(document.getElementById("item3").value) - 1;
    if (document.getElementById("item3").value <= parseInt(min)) {
        document.getElementById("item3").value = min;
    }
}

function up4(max) {
    document.getElementById("item4").value = parseInt(document.getElementById("item4").value) + 1;
    if (document.getElementById("item4").value >= parseInt(max)) {
        document.getElementById("item4").value = max;
    }
}
function down4(min) {
    document.getElementById("item4").value = parseInt(document.getElementById("item4").value) - 1;
    if (document.getElementById("item4").value <= parseInt(min)) {
        document.getElementById("item4").value = min;
    }
}
//ACCEPT NUM ONLY 
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
//CART1
var counter = 1;
var sum = 0;
var total = 0;
var input2;

function cart1(){
    var input1 = document.getElementById('item1').value;

    if (input1 % 3 === 0){ //check if divisible by 3
        var divide = input1/3;
        var difference = input1 - divide;
        total = difference * 50;
    }else{        
        total = input1 * 50;
    }

    sum = sum+total;
    var latestitem = document.getElementById('item').innerHTML;
    document.getElementById('scenario').innerHTML = counter;
    var items = document.getElementById('item').innerHTML = latestitem + input1 +' x Nike Cortez <br/>';
    total = document.getElementById('total').innerHTML = "$ "+sum;
    var cart = document.getElementById('cart').innerHTML = latestitem + input1+' x Nike Cortez <br/>';
    document.getElementById('item1').value = 1;
}

function cart2(){
    var input3 = document.getElementById('item3').value;

    if (input3 > 3){
        total = input3 * 55;
    }else{
        total = input3 * 70;
    }

    sum = sum+total;
    var latestitem = document.getElementById('item').innerHTML;
    document.getElementById('scenario').innerHTML = counter;
    items = document.getElementById('item').innerHTML = latestitem + input3 +' x New Balance 576 <br/>';
    var total = document.getElementById('total').innerHTML = "$ "+sum;
    cart = document.getElementById('cart').innerHTML = latestitem + input3+' x New Balance 576 <br/>';
    document.getElementById('item3').value = 1;
}

function cart3(){
    input2 = document.getElementById('item2').value;

    total = input2 * 60;

    sum = sum+total;
    var free = input2+' x Havaianas <br>';
    var latestitem = document.getElementById('item').innerHTML;
    //var withfree = document.getElementById('cart').innerHTML + free;
    document.getElementById('scenario').innerHTML = counter;
    items = document.getElementById('item').innerHTML = latestitem + input2 +' x Adidas Superstar <br/>';
    var total = document.getElementById('total').innerHTML = "$ "+sum;
    cart = document.getElementById('cart').innerHTML = latestitem + free + input2+' x Adidas Superstar ';
    document.getElementById('item2').value = 1;
}

function cart4(){
    var input4 = document.getElementById('item4').value;
    total = input4 * 20;

    sum = sum+total;
    var latestitem = document.getElementById('item').innerHTML;
    document.getElementById('scenario').innerHTML = counter;
    items = document.getElementById('item').innerHTML = latestitem + input4 +' x Havaianas <br/>';
    var total = document.getElementById('total').innerHTML = "$ "+sum;
    cart = document.getElementById('cart').innerHTML = latestitem + input4+' x Havaianas ';
    document.getElementById('item4').value = 1;
}


function discount(){
        var retVal = prompt("Enter the Promo Code to get 10% discount: ", "Promo Code here");
        console.log(retVal);

        if(retVal == 'SNEAKERH3AD'){
           var discount = sum * .10;
           var sub = sum - discount;
           total = document.getElementById('total').innerHTML = "$ "+sub;
           alert('Great! you will receive 10% dicount');
        }else{
            total = document.getElementById('total').innerHTML = "$ "+sum;
            alert("Ooops! the Promo code you've entered is invalid");
        }
}

function checkout(){
    counter++;
    document.getElementById('scenario').innerHTML = counter;
    document.getElementById('item').innerHTML = '';
    document.getElementById('total').innerHTML = '';
    document.getElementById('cart').innerHTML = '';
    alert('Awesome! Your items is on process now, wait for our message regarding the delivery. ');
    sum = 0; total = 0;
}

function remove(){
     document.getElementById('item').innerHTML = '';
     document.getElementById('total').innerHTML = '';
     document.getElementById('cart').innerHTML = '';
}
